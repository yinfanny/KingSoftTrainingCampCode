#include <iostream>
#include <memory>

using namespace std;

void asSmartPointerGood(std::shared_ptr<int>& shr)
{
    cout << "2 : shr.use_count()=" << shr.use_count() << ", data=  " << *shr << endl; 
    shr.reset(new int(10));
    int cut = shr.use_count();
    cout << "3 : shr.use_count()=" << shr.use_count() << ", data=  " << *shr << endl;
}

void asSmartPointerBad(std::shared_ptr<int>& shr)
{
    cout << "2 : shr.use_count()=" << shr.use_count() << ", data=  " << *shr << endl; 
    *shr += 90;
    cout << "3 : shr.use_count()=" << shr.use_count() << ", data=  " << *shr << endl;
}

void SmartPointerGood(std::shared_ptr<int> shr)
{
    cout << "2 : shr.use_count()=" << shr.use_count() << ", data=  " << *shr << endl;
    shr.reset(new int(10));
    int cut = shr.use_count();
    cout << "3 : shr.use_count()=" << shr.use_count() << ", data=  " << *shr << endl;
}

void SmartPointer(std::shared_ptr<int> shr)
{
    *shr += 90;
    cout << "2 : shr.use_count()=" << shr.use_count() << ", data=  " << *shr << endl;
}


int main()
{
    shared_ptr<int> ptr = make_shared<int>(int(5));
    cout << "1 : ptr.use_count()=" <<  ptr.use_count() << ", data=  "<< *ptr <<endl;

    //SmartPointer(ptr);
    //SmartPointerGood(ptr);
    //asSmartPointerBad(ptr);
    asSmartPointerGood(ptr);

    cout << "4 : ptr.use_count()=" << ptr.use_count() << ", data=  " << *ptr << endl;
    return 0;
}