#include <iostream>
using namespace std;

class KTryClass
{
public:
    //无参构造
    KTryClass()
    {        
        m_data = nullptr;
        m_data = new char[6];
        memcpy(m_data, "hello", 6);
        cout << "KTryClass(),this =" << this << ";   m_data address =" << static_cast<void*>(m_data) << endl;
    }

    //拷贝构造
    KTryClass(const KTryClass& other)
    {
        m_data = new char[6];
        memcpy(m_data, other.m_data, 6);
        cout << "KTryClass(const KTryClass& kt),this =" << this << ";   m_data address =" << static_cast<void*>(m_data) << endl;
    }

    //移动构造函数
    //添加移动构造函数，getDeadObj()的返回值是一个将亡值，也就是一个右值
    //在移动构造中如果使用了右值引用，会将临时对象堆区的地址所有权转给新对象。
    //这块内存的所有权被转移，但是内存依旧存在
    /*   
    KTryClass(KTryClass && other)
    {
        if (other.m_data != nullptr)
        {
            m_data = other.m_data;
            other.m_data = nullptr;
        }
        cout << " KTryClass(KTryClass && kt),this =" << this << ";   m_data address =" << static_cast<void*>(m_data) << endl;
    }
   */

   //析构函数
    ~KTryClass()
    {
        cout << " ~KTryClass(),this =" << this << ";   m_data address =" << static_cast<void*>(m_data) << endl;
        delete[] m_data;
        m_data = nullptr;
    }

    char* getAddr() const
    {
        return m_data;
    }

private:
    char* m_data = nullptr;
};

//得到将亡对象，返回的时候系统会进行拷贝构造

KTryClass getDeadObj()
{
    KTryClass  obj;
    cout << "local param: obj address =" << &obj  << ",  m_data address = " << static_cast<void*>(obj.getAddr()) << endl;
    return obj;
}

int main()
{
    //系统会调用移动拷贝构造
    KTryClass  kt = getDeadObj();
    cout << "main:  new obj addr = " << &kt << ",  m_data address = " << static_cast<void*>(kt.getAddr()) << endl;
    return 0;
}
