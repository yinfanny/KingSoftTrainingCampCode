#include <iostream>
#include <string>
#include <memory>

using std::shared_ptr;
using std::make_shared;

struct Widget
{
    Widget(int d) :data(d) {};
    int data;
};

auto shareptr1 = std::make_shared<Widget>(2000);

void oldfoo(Widget& widget)
{
    //widget do something
    widget.data = 99;
    std::cout << "3 :  形参widget.data=" << widget.data << std::endl;
    std::cout << "3 :  全局shareptr1.use_count()=" << shareptr1.use_count() << ", data=  " << shareptr1->data << std::endl;
    
}

void shared(std::shared_ptr<Widget>& sharePtr)
{
    std::cout << "2 :  全局shareptr1.use_count()=" << shareptr1.use_count() << ", data=  " << shareptr1->data << std::endl;
    std::cout << "2 :  形参shareptr.use_count()=" << sharePtr.use_count() << ", data=  " << sharePtr->data << std::endl;
    oldfoo(*sharePtr);
    std::cout << "4 : 全局shareptr1.use_count()=" << shareptr1.use_count() << ", data=  " << sharePtr->data << std::endl;
    std::cout << "4 :  形参参shareptr.use_count()=" << sharePtr.use_count() << ", data=  " << sharePtr->data << std::endl;
}



int main()
{
    std::cout << "1 : 全局shareptr1.use_count()=" << shareptr1.use_count() << ", data=  " << shareptr1->data << std::endl;
    shared(shareptr1);
    std::cout << "5 : 全局shareptr1.use_count()=" << shareptr1.use_count() << ", data=  " << shareptr1->data << std::endl;
    return 0;
}